# Avris Stringer

A set of useful, string-related helpers.

## Installation

    composer require avris/stringer

## Usage

    $stringer = (new LocalisatorBuilder())
        ->registerExtension(new LocalisatorExtension('en'))
        ->registerExtension(new StringerExtension())
        ->build(Stringer::class);
        
If you register `StringerTwig` as a [Twig](https://twig.symfony.com/) extension,
you can use all of the following helpers as Twig filters, for example:

    echo $stringer->timeDiff('2017-03-04 20:56');
    
can also be used as:

    {{ '2017-03-04 20:56'|timeDiff }} 

Some of the helpers need to be tailored to a specific language.
English is built in and Polish can be added by [Avris Polonisator](https://gitlab.com/Avris/Polonisator).
Check it out as a reference to adding more languages.

### convertCase

**convertCase** converts strings to several programistic conventions:

* `$stringer->convertCase('title')` → `Title Case`
* `$stringer->convertCase('camel')` → `camelCase`
* `$stringer->convertCase('pascal')` → `PascalCase`
* `$stringer->convertCase('underscore')` → `underscore_case`

Credits to [Avro](https://github.com/jdewit/AvroCaseBundle)

### clearUrl

A URL address treated with **clearUrl** will be trimmed of all the parts that don't have to be shown to the user:

	<a href="{{ post.source }}" target="_blank">{{ post.source|clearUrl }}</a>

For `https://9gag.com/gag/aVOBPxn?ref=fsidebar` this code will produce a link to a valid URL,
but displayed as `9gag.com/gag/aVOBPxn`.

### numberInWords

Function **numberInWords** accepts a number and returns a string with its representation in words:

	375 → three hundred seventy-five
    -279 → minus two hundred seventy-nine
    146 → one hundred fourty-six
    10000000 → ten million
    5371.18 → five thousand three hundred seventy-one and eighteen hundrenth
    5678 → five thousand six hundred seventy-eight
    0.1 → one tenth
    0.000908 → zero comma zero zero zero nine zero eight

### timeInWords

Function **numberInWords** accepts a time (`\DateTime` or string parsable by `\DateTime`)
and returns a string reading out this time. There are three modes available, with increasing details.

`$stringer->timeInWords($time, TimeInWords::MODE_NICE)` (default):

    14:15:16 → quarter past two pm
    05:13:32 → thirteen past five am
    19:49:20 → eleven to eight pm
    11:53:11 → seven to noon

`$stringer->timeInWords($time, TimeInWords::MODE_SHORT)`:

    14:15:16 → two fifteen pm
    05:13:32 → five thirteen am
    19:49:20 → seven fourty-nine pm
    11:53:11 → eleven fifty-three pm

`$stringer->timeInWords($time, TimeInWords::MODE_LONG)`:

    14:15:16 → two fifteen pm and sixteen seconds
    05:13:32 → five thirteen am and thirty-two seconds
    19:49:20 → seven fourty-nine pm and twenty seconds
    11:53:11 → eleven fifty-three pm and eleven seconds

### timeDiff

Function **timeDiff** accepts a datetime (`\DateTime` or string parsable by `\DateTime`)
and returns a string explaining how long ago it was / how far in the future it is.
You can change the point of reference from `"now"` to a different date by specifying the second parameter.

    echo $stringer->timeDiff('2018-03-04 20:56');

Will echo a text like "now", "5 minutes ago", "2 hours ago" etc., depending on current time.

### listify

Function **listify** takes a list of elements and a number, how many of them can be displayed at most
(default `0`, meaning no limit), while the rest will be replaced by "and X others" or similar, for example:

    $stringer->listify(['A','B','C','D','E','F'], 1);   // '6 people commented'
    $stringer->listify(['A','B','C','D','E','F'], 2);   //  'A and 5 others commented'
    $stringer->listify(['A','B','C','D','E','F'], 3);   //  'A, B and 4 others commented'
    $stringer->listify(['A','B','C','D','E','F'], 4);   //  'A, B, C and 3 others commented'
    $stringer->listify(['A','B','C','D','E','F'], 5);   //  'A, B, C, D and 2 others commented'
    $stringer->listify(['A','B','C','D','E','F'], 6);   //  'A, B, C, D, E and F commented'
    $stringer->listify(['A','B','C','D','E','F'], 7);   //  'A, B, C, D, E and F commented'
    $stringer->listify(['A','B','C','D','E','F'], 8);   //  'A, B, C, D, E and F commented'
    $stringer->listify(['A','B','C','D','E','F'], 0);   //  'A, B, C, D, E and F commented'
    $stringer->listify(['A'], 0);                       //  'A commented'
    $stringer->listify(['A', 'B'], 0);                  //  'A and B commented'

The elements of the array can be either strings or objects with `__toString` defined.

By default the "people commented" translation is used, but you can change the translation keys with the third parameter:

    $stringer->listify($loadedProcesses, 3, 'app:answer.listify.');

and in `app.en.yml`:

    answer:
      listify:
        simple: 'loaded'
        addition: '<> one process loaded|%count% processes loaded'
        others: '<> one other process loaded|%count% other processes loaded'
        and: 'and'
 

### phone

Function **phone** provides a nice formatting of a phone number:

* it determines the country code (and normalises`00` to `+`),
  and if it's not given, but you provided a default locale as a second parameter,
  it assumes the country code for that country,
* if there is a `PhoneLocaleFormatter` registered for that country code,
  the phone number is further formatted, 
  for instance for `pl_PL` / `+48` from [Avris Polonisator](https://gitlab.com/Avris/Polonisator) 
   * it removes the optional `0` at the beginning
   * splits the number into correct blocks
     (mobile and special numbers – three blocks of three digits; landline – into city code and the actual number).


    0048501123456 → +48 501 123 456
    48911234567 → +48 91 1234567
    0911234567 → 91 1234567
    0202224 → 20 2224
    001555123456 → +1 555123456
    +69012345 → +690 12345

### arabicToRoman

Function **arabicToRoman** converts natural numbers from range 1-3999 to roman.

## Framework integration

### Micrus

In your `App\App:registerModules` register the Localisator module and the Stringer module:

    yield new \Avris\Localisator\LocalisatorModule;
    yield new \Avris\Stringer\StringerModule;
    
And it should work out of the box: `Avris\Stringer\Stringer` available in the container and the Twig extension registered.
    
### Symfony

Example `servies.yaml` config:

    Avris\Localisator\LocalisatorBuilder:
        calls:
            - [registerExtension, ['@Avris\Localisator\LocalisatorExtension']]
            - [registerExtension, ['@Avris\Stringer\StringerExtension']]
    Avris\Localisator\LocalisatorExtension:
        arguments:
            $locale: '%locale%'
    Avris\Stringer\StringerExtension: ~

    Avris\Localisator\LocalisatorInterface:
        factory: ['@Avris\Localisator\LocalisatorBuilder', build]
        arguments: ['Avris\Localisator\LocalisatorInterface']
    Avris\Localisator\LocalisatorTwig: ~

    Avris\Stringer\Stringer:
        factory: ['@Avris\Localisator\LocalisatorBuilder', build]
        arguments: ['Avris\Stringer\Stringer']
    Avris\Stringer\StringerTwig:
        public: true

### Copyright ###

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
