<?php
namespace Avris\Stringer\Service;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\Service\CaseConverter
 */
final class CaseConverterTest extends BaseTest
{
    const INPUTS = [
        'lorem ipsum dolor',
        'Lorem Ipsum Dolor',
        'loremIpsumDolor',
        'LoremIpsumDolor',
        'lorem_ipsum_dolor',
    ];

    const OUTPUTS = [
        CaseConverter::CASE_TITLE => 'Lorem Ipsum Dolor',
        CaseConverter::CASE_CAMEL => 'loremIpsumDolor',
        CaseConverter::CASE_PASCAL => 'LoremIpsumDolor',
        CaseConverter::CASE_UNDERSCORE => 'lorem_ipsum_dolor',
    ];

    /**
     * @dataProvider caseProvider
     */
    public function testConvertCase($input, $format, $output)
    {
        $this->assertEquals($output, self::$stringer->convertCase($input, $format));
    }

    public function caseProvider()
    {
        foreach (self::INPUTS as $input) {
            foreach (self::OUTPUTS as $format => $output) {
                yield [$input, $format, $output];
            }
        }
    }

    /**
     * @dataProvider guessCaseProvider
     */
    public function testGuessCase($format, $string)
    {
        $this->assertEquals($format, self::$stringer->guessCase($string));
    }

    public function guessCaseProvider()
    {
        foreach (self::OUTPUTS as $format => $string) {
            yield [$format, $string];
        }
    }
}
