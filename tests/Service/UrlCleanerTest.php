<?php
namespace Avris\Stringer\Service;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\Service\UrlCleaner
 */
class UrlCleanerTest extends BaseTest
{
    /**
     * @dataProvider cleanUrlProvider
     */
    public function testClearUrl($input, $output)
    {
        $this->assertEquals($output,self::$stringer->clearUrl($input));
    }

    public function cleanUrlProvider()
    {
        return [
            ['http://9gag.com/gag/aVOBPxn?ref=fsidebar', '9gag.com/gag/aVOBPxn'],
            ['https://avris.it/     ', 'avris.it'],
            ['https://avris.it/?foo=bar', 'avris.it'],
        ];
    }
}
