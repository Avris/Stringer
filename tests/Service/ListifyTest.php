<?php
namespace Avris\Stringer\Service;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\Service\Listify
 */
class ListifyTest extends BaseTest
{
    const COMMENTATORS = ['A','B','C','D','E','F'];
    
    /**
     * @dataProvider listifyProvider
     */
    public function testListify($commentators, $maxShown, $outputNoDesc, $outputDesc)
    {
        $this->assertEquals($outputNoDesc, self::$stringer->listify($commentators, $maxShown, 'nonexistent'));
        $this->assertEquals($outputDesc, self::$stringer->listify($commentators, $maxShown));
    }

    public function listifyProvider()
    {
        return [
            [self::COMMENTATORS, 1, '6', '6 people commented'],
            [self::COMMENTATORS, 2, 'A and 5', 'A and 5 others commented'],
            [self::COMMENTATORS, 3, 'A, B and 4', 'A, B and 4 others commented'],
            [self::COMMENTATORS, 4, 'A, B, C and 3', 'A, B, C and 3 others commented'],
            [self::COMMENTATORS, 5, 'A, B, C, D and 2', 'A, B, C, D and 2 others commented'],
            [self::COMMENTATORS, 6, 'A, B, C, D, E and F', 'A, B, C, D, E and F commented'],
            [self::COMMENTATORS, 7, 'A, B, C, D, E and F', 'A, B, C, D, E and F commented'],
            [self::COMMENTATORS, 8, 'A, B, C, D, E and F', 'A, B, C, D, E and F commented'],
            [self::COMMENTATORS, 0, 'A, B, C, D, E and F', 'A, B, C, D, E and F commented'],
            [['A'], 0, 'A', 'A commented'],
            [['A', 'B'], 0, 'A and B', 'A and B commented'],
        ];
    }
}
