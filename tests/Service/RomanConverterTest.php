<?php
namespace Avris\Stringer\Service;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\Service\RomanConverter
 */
class RomanConverterTest extends BaseTest
{
    /**
     * @dataProvider romanProvider
     */
    public function testArabicToRomanConvert($arabic, $expected)
    {
        $romanNumber = self::$stringer->arabicToRoman($arabic);
        $this->assertRegExp('/^[IVXLCDM]+$/', $romanNumber);
        $this->assertEquals($expected, $romanNumber);
    }

    public function romanProvider()
    {
        return [
            [1, 'I'],
            [2, 'II'],
            [3, 'III'],
            [4, 'IV'],
            [5, 'V'],
            [6, 'VI'],
            [7, 'VII'],
            [8, 'VIII'],
            [9, 'IX'],
            [10, 'X'],
            [11, 'XI'],
            [15, 'XV'],
            [19, 'XIX'],
            [20, 'XX'],
            [21, 'XXI'],
            [40, 'XL'],
            [50, 'L'],
            [77, 'LXXVII'],
            [99, 'XCIX'],
            [100, 'C'],
            [150, 'CL'],
        ];
    }

    /**
     * @dataProvider exceptionsProvider
     */
    public function testExceptions($input, $message)
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        self::$stringer->arabicToRoman($input);
    }

    public function exceptionsProvider()
    {
        yield [-1, 'Input value must be 1 or greater'];
        yield [0, 'Input value must be 1 or greater'];
        yield [4000, 'Input value must be 3999 or less'];
    }
}
