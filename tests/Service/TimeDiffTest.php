<?php
namespace Avris\Stringer\Service;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\Service\TimeDiff
 */
class TimeDiffTest extends BaseTest
{
    const BASE = '2016-12-17 8:00';

    /**
     * @dataProvider diffProvider
     */
    public function testDiff($date, $expected)
    {
        $this->assertEquals(
            $expected,
            self::$stringer->timeDiff($date, self::BASE)
        );
    }

    public function diffProvider()
    {
        yield ['2013-03-05 5:00', '4 years ago'];
        yield ['2016-03-05 5:00', '9 months ago'];
        yield ['2016-12-05 5:00', '12 days ago'];
        yield ['2016-12-13 19:00', '4 days ago'];
        yield ['2016-12-15 15:00', '2 days ago'];
        yield ['2016-12-16 15:00', 'yesterday'];
        yield ['2016-12-17 5:00', 'about 3 hours ago'];
        yield ['2016-12-17 6:30', 'about an hour and a half ago'];
        yield ['2016-12-17 7:00', 'about an hour ago'];
        yield ['2016-12-17 7:30', 'about half an hour ago'];
        yield ['2016-12-17 7:55', '5 minutes ago'];
        yield ['2016-12-17 7:59', 'a minute ago'];
        yield ['2016-12-17 7:59:45', 'now'];
        yield ['2016-12-17 8:01', 'in a minute'];
        yield ['2016-12-17 8:04', 'in 4 minutes'];
        yield ['2016-12-17 8:42', 'in about half an hour'];
        yield ['2016-12-17 9:01', 'in about an hour'];
        yield ['2016-12-17 15:12', 'in about 7 hours'];
        yield ['2016-12-18 15:00', 'tomorrow'];
        yield ['2016-12-19 15:00', 'in 2 days'];
        yield ['2016-12-24 15:12', 'in 7 days'];
        yield ['2017-05-24 15:12', 'in 5 months'];
        yield ['2023-12-24 15:12', 'in 7 years'];
    }
}
