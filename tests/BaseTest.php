<?php
namespace Avris\Stringer;

use Avris\Localisator\LocalisatorBuilder;
use Avris\Localisator\LocalisatorExtension;
use Avris\Polonisator\PolonisatorExtension;
use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    /** @var Stringer */
    protected static $stringer;

    public static function setUpBeforeClass()
    {
        $builder = (new LocalisatorBuilder())
            ->registerExtension(new LocalisatorExtension('en'))
            ->registerExtension(new StringerExtension())
            ->registerExtension(new PolonisatorExtension())
        ;

        self::$stringer = $builder->build(Stringer::class);
    }
}
