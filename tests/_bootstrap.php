<?php
require __DIR__ . '/../vendor/autoload.php';

foreach (glob(__DIR__ . '/_help/*.php') as $file) {
    require $file;
}
