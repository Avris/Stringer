<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\LocaleFormatter\EnglishFormatter
 * @covers \Avris\Stringer\LocaleFormatter\EnglishNumberInWords
 * @covers \Avris\Stringer\Service\NumberInWords
 * @covers \Avris\Stringer\LocaleFormatter\MatchLocaleFormatter
 */
class EnglishNumberInWordsTest extends BaseTest
{
    /**
     * @dataProvider numberInWordsProvider
     */
    public function testNumberInWords($input, $output)
    {
        $this->assertEquals($output, self::$stringer->numberInWords($input));
    }

    public function numberInWordsProvider()
    {
        return [
            [468, 'four hundred sixty-eight'],
            [1017, 'thousand seventeen'],
            [-37, 'minus thirty-seven'],
            [324, 'three hundred twenty-four'],
            [10000000, 'ten million'],
            [4471.952, 'four thousand four hundred seventy-one and nine hundred fifty-two thousandth'],
            [5.8181, 'five comma eight one eight one'],
            [0.000385, 'zero comma zero zero zero three eight five'],
            ['5.2E-13', 'zero comma zero zero zero zero zero zero zero zero zero zero zero zero five two'],
        ];
    }
}
