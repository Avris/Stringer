<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Stringer\Service\TimeInWords;
use Avris\Stringer\BaseTest;

/**
 * @covers \Avris\Stringer\Stringer
 * @covers \Avris\Stringer\LocaleFormatter\EnglishFormatter
 * @covers \Avris\Stringer\LocaleFormatter\EnglishTimeInWords
 * @covers \Avris\Stringer\Service\TimeInWords
 * @covers \Avris\Stringer\LocaleFormatter\MatchLocaleFormatter
 * @covers \Avris\Stringer\Entity\Time
 */
class EnglishTimeInWordsTest extends BaseTest
{
    /**
     * @dataProvider timesProvider
     */
    public function testTimeInWords($timeString, $modeNice, $modeShort, $modeLong)
    {
        $this->assertEquals($modeNice, self::$stringer->timeInWords($timeString, TimeInWords::MODE_NICE));
        $this->assertEquals($modeShort, self::$stringer->timeInWords($timeString, TimeInWords::MODE_SHORT));
        $this->assertEquals($modeLong, self::$stringer->timeInWords($timeString, TimeInWords::MODE_LONG));
    }

    public function timesProvider()
    {
        return [
            ['02:23:07', 'twenty-three past two am', 'two twenty-three am', 'two twenty-three am and seven seconds'],
            ['21:32:08', 'thirty-two past nine pm', 'nine thirty-two pm', 'nine thirty-two pm and eight seconds'],
            ['16:31:34', 'thirty-one past four pm', 'four thirty-one pm', 'four thirty-one pm and thirty-four seconds'],
            ['16:44:27', 'sixteen to five pm', 'four fourty-four pm', 'four fourty-four pm and twenty-seven seconds'],
            ['21:24:45', 'twenty-four past nine pm', 'nine twenty-four pm', 'nine twenty-four pm and fourty-five seconds'],
            ['21:00:00', 'nine pm', 'nine pm', 'nine o\'clock pm and zero seconds'],
            ['12:00:45', 'noon', 'twelve pm', 'twelve o\'clock pm and fourty-five seconds'],
            ['12:10:00', 'ten past noon', 'twelve ten pm', 'twelve ten pm and zero seconds'],
            ['00:00:00', 'midnight', 'twelve am', 'twelve o\'clock am and zero seconds'],
            ['23:50:00', 'ten to midnight', 'eleven fifty pm', 'eleven fifty pm and zero seconds'],
            ['18:44:06', 'sixteen to seven pm', 'six fourty-four pm', 'six fourty-four pm and six seconds'],
            ['17:04:48', 'four past five pm', 'five o four pm', 'five o four pm and fourty-eight seconds'],
            ['03:05:25', 'five past three am', 'three o five am', 'three o five am and twenty-five seconds'],
            ['08:21:49', 'twenty-one past eight am', 'eight twenty-one am', 'eight twenty-one am and fourty-nine seconds'],
            ['10:02:05', 'two past ten am', 'ten o two am', 'ten o two am and five seconds'],
            ['02:29:34', 'twenty-nine past two am', 'two twenty-nine am', 'two twenty-nine am and thirty-four seconds'],
            ['02:15:34', 'quarter past two am', 'two fifteen am', 'two fifteen am and thirty-four seconds'],
            ['02:30:34', 'half past two am', 'two thirty am', 'two thirty am and thirty-four seconds'],
            ['02:45:34', 'quarter to three am', 'two fourty-five am', 'two fourty-five am and thirty-four seconds'],
            ['02:59:34', 'a minute to three am', 'two fifty-nine am', 'two fifty-nine am and thirty-four seconds'],
        ];
    }
}
