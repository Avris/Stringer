<?php
namespace Avris\Polonisator;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;
use Avris\Polonisator\LocaleFormatter\PolishPhoneFormatter;

class PolonisatorExtension implements ContainerBuilderExtension
{
    public function extend(ContainerInterface $container)
    {
        $container->setDefinition(PolishPhoneFormatter::class, ['tags' => 'phoneLocaleFormatter']);
    }
}
