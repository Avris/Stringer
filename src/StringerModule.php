<?php
namespace Avris\Stringer;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class StringerModule implements ModuleInterface
{
    use ModuleTrait;
}
