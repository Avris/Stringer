<?php
namespace Avris\Stringer;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;
use Avris\Localisator\LocalisatorInterface;
use Avris\Stringer\LocaleFormatter\EnglishNumberInWords;
use Avris\Stringer\LocaleFormatter\EnglishTimeInWords;
use Avris\Stringer\Service\CaseConverter;
use Avris\Stringer\Service\Listify;
use Avris\Stringer\Service\NumberInWords;
use Avris\Stringer\Service\PhoneFormatter;
use Avris\Stringer\Service\RomanConverter;
use Avris\Stringer\Service\TimeDiff;
use Avris\Stringer\Service\TimeInWords;
use Avris\Stringer\Service\UrlCleaner;

/**
 * @codeCoverageIgnore
 */
class StringerExtension implements ContainerBuilderExtension
{
    const DIR = __DIR__ . '/../translations';

    public function extend(ContainerInterface $container)
    {
        $container->setDefinition(sha1(self::DIR), [
            'resolve' => self::DIR,
            'tags' => 'translationDir',
        ]);

        $container->setDefinition(Stringer::class, [
            'class' => Stringer::class,
            'arguments' => [
                '$listify' => '@' . Listify::class,
                '$caseConverter' => '@' . CaseConverter::class,
                '$romanConverter' => '@' . RomanConverter::class,
                '$urlCleaner' => '@' . UrlCleaner::class,
                '$phoneFormatter' => '@' . PhoneFormatter::class,
                '$numberInWords' => '@' . NumberInWords::class,
                '$timeInWords' => '@' . TimeInWords::class,
                '$timeDiff' => '@' . TimeDiff::class,
            ],
            'public' => true,
        ]);

        $container->setDefinition(EnglishNumberInWords::class, [
            'tags' => 'numberInWordsLocaleFormatter',
        ]);

        $container->setDefinition(EnglishTimeInWords::class, [
            'arguments' => [
                '$numberInWords' => '@' . NumberInWords::class,
            ],
            'tags' => 'timeInWordsLocaleFormatter',
        ]);

        $container->setDefinition(Listify::class, [
            'arguments' => [
                '$localisator' => '@' . LocalisatorInterface::class,
            ],
        ]);
        $container->setDefinition(CaseConverter::class, []);
        $container->setDefinition(RomanConverter::class, []);
        $container->setDefinition(UrlCleaner::class, []);
        $container->setDefinition(PhoneFormatter::class, [
            'arguments' => [
                '$phoneLocaleFormatters' => '#phoneLocaleFormatter',
            ],
        ]);

        $container->setDefinition(NumberInWords::class, [
            'arguments' => [
                '$localisator' => '@' . LocalisatorInterface::class,
                '$orderProvider' => '@' . LocalisatorInterface::class . '.order',
                '$numberInWordsLocaleFormatters' => '#numberInWordsLocaleFormatter',
            ],
        ]);

        $container->setDefinition(TimeInWords::class, [
            'arguments' => [
                '$orderProvider' => '@' . LocalisatorInterface::class . '.order',
                '$timeInWordsLocaleFormatters' => '#timeInWordsLocaleFormatter',
            ],
        ]);

        $container->setDefinition(TimeDiff::class, [
            'arguments' => [
                '$localisator' => '@' . LocalisatorInterface::class,
            ],
        ]);
    }
}
