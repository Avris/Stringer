<?php
namespace Avris\Stringer;

use Avris\Stringer\Service\TimeInWords;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * @codeCoverageIgnore
 */
final class StringerTwig extends AbstractExtension
{
    /** @var Stringer */
    private $stringer;

    public function __construct(Stringer $stringer)
    {
        $this->stringer = $stringer;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('listify', function (array $array, int $maxShown = 0, $base = 'stringer:listify.'): string {
                return $this->stringer->listify($array, $maxShown, $base);
            }),
            new TwigFilter('convertCase', function (string $string, string $format): string {
                return $this->stringer->convertCase($string, $format);
            }),
            new TwigFilter('arabicToRoman', function (int $arabic) {
                return $this->stringer->arabicToRoman($arabic);
            }),
            new TwigFilter('clearUrl', function (string $url): string {
                return $this->stringer->clearUrl($url);
            }),
            new TwigFilter('phone', function ($number, $defaultLocale = null): string {
                return $this->stringer->phone($number, $defaultLocale);
            }),
            new TwigFilter('numberInWords', function ($number, int $gender = Stringer::MASCULINE): string {
                return $this->stringer->numberInWords($number, $gender);
            }),
            new TwigFilter('timeInWords', function ($time, $mode = TimeInWords::MODE_NICE): string {
                return $this->stringer->timeInWords($time, $mode);
            }),
            new TwigFilter('timeDiff', function ($datetime, $now = null): string {
                return $this->stringer->timeDiff($datetime, $now);
            }),
        ];
    }
}
