<?php
namespace Avris\Stringer\LocaleFormatter;

abstract class EnglishFormatter implements LocaleFormatter
{
    public function getLocales(): array
    {
        return [
            'en', 'en_AU', 'en_BZ', 'en_CA', 'en_GB', 'en_IE', 'en_IN', 'en_JM',
            'en_MY', 'en_NZ', 'en_PH', 'en_SG', 'en_TT', 'en_US', 'en_ZA', 'en_ZW',
        ];
    }
}
