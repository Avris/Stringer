<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Stringer\Entity\Time;
use Avris\Stringer\Service\NumberInWords;
use Avris\Stringer\Stringer;

final class EnglishTimeInWords extends EnglishFormatter implements TimeInWordsLocaleFormatter
{
    /** @var NumberInWords */
    private $numberInWords;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(NumberInWords $numberInWords)
    {
        $this->numberInWords = $numberInWords;
    }

    /**
     * quarter to two am
     */
    public function getNice(Time $time): string
    {
        if ($time->getMinutes() === 0) {
            return $this->describeHoursNice($time->getHours());
        }

        if ($time->getMinutes() === 30) {
            return 'half past ' . $this->describeHoursNice($time->getHours());
        }

        if ($time->getMinutes() < 40) {
            return $this->describeMinutesNice($time->getMinutes())
                . ' past '
                . $this->describeHoursNice($time->getHours());
        }

        return  $this->describeMinutesNice(60 - $time->getMinutes())
            . ' to '
            . $this->describeHoursNice($time->getHoursNext());
    }

    private function describeMinutesNice(int $minutes)
    {
        if ($minutes === 1) {
            return 'a minute';
        }

        if ($minutes === 15) {
            return 'quarter';
        }

        return $this->numberInWords->numberInWords($minutes);
    }

    private function describeHoursNice(int $hours)
    {
        if ($hours === 0) {
            return 'midnight';
        }

        if ($hours === 12) {
            return 'noon';
        }

        return sprintf(
            '%s %s',
            $this->describeHours($hours),
            $this->describeAmPm($hours)
        );
    }

    /**
     * one fourty-five am
     */
    public function getShort(Time $time): string
    {
        return join(' ', array_filter([
            $this->describeHours($time->getHours()),
            $this->describeMinutes($time->getMinutes()),
            $this->describeAmPm($time->getHours()),
        ]));
    }

    /**
     * one fourty-five am and fourty seconds
     */
    public function getLong(Time $time): string
    {
        return trim(sprintf(
            '%s %s %s and %s second%s',
            $this->describeHours($time->getHours()),
            $this->describeMinutes($time->getMinutes()) ?: 'o\'clock',
            $this->describeAmPm($time->getHours()),
            $this->describeSeconds($time->getSeconds()) ?: 'zero',
            $time->getSeconds() === 1 ? '' : 's'
        ));
    }

    private function describeHours(int $hours): string
    {
        $hours = $hours % 12 ?: 12;

        return $this->numberInWords->numberInWords($hours);
    }

    private function describeMinutes(int $minutes): string
    {
        if (!$minutes) {
            return '';
        }

        $o = strlen($minutes) === 1 ? 'o ' : '';

        return $o . $this->numberInWords->numberInWords($minutes);
    }

    private function describeAmPm(int $hours): string
    {
        return 0 <= $hours && $hours < 11 ? 'am' : 'pm';
    }

    private function describeSeconds($seconds): string
    {
        return $seconds ? $this->numberInWords->numberInWords($seconds, Stringer::FEMININE) : '';
    }
}
