<?php
namespace Avris\Stringer\LocaleFormatter;

interface PhoneLocaleFormatter extends LocaleFormatter
{
    public function getCode(): string;

    public function format($number): string;
}
