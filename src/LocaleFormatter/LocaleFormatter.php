<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Localisator\Locale\LocaleInterface;

interface LocaleFormatter
{
    /**
     * @return string[]|LocaleInterface[]
     */
    public function getLocales(): array;
}
