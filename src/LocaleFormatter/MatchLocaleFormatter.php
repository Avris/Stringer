<?php
namespace Avris\Stringer\LocaleFormatter;

/**
 * @property \Avris\Localisator\Order\LocaleOrderProviderInterface $orderProvider
 * @property \Avris\Stringer\LocaleFormatter\LocaleFormatter[] $formatters
 */
trait MatchLocaleFormatter
{
    private function matchFormatter()
    {
        foreach ($this->orderProvider->getOrder() as $locale) {
            foreach ($this->formatters as $formatter) {
                foreach ($formatter->getLocales() as $formatterLocale) {
                    if ((string) $locale === (string) $formatterLocale) {
                        return $formatter;
                    }
                }
            }
        }

        throw new \RuntimeException('No LocaleFormatter supported for given LocaleOrder'); // @codeCoverageIgnore
    }
}
