<?php
namespace Avris\Stringer\LocaleFormatter;

interface NumberInWordsLocaleFormatter extends LocaleFormatter
{
    public function wordifyWhole($whole, int $gender);

    public function wordifyDecimal($decimal, int $gender);
}
