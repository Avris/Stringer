<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Stringer\Entity\Time;

interface TimeInWordsLocaleFormatter extends LocaleFormatter
{
    public function getNice(Time $time): string;

    public function getShort(Time $time): string;

    public function getLong(Time $time): string;
}
