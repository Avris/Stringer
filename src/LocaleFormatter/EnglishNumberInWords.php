<?php
namespace Avris\Stringer\LocaleFormatter;

final class EnglishNumberInWords extends EnglishFormatter implements NumberInWordsLocaleFormatter
{
    const ONES = [
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
    ];

    const TEENS = [
        1 => 'eleven',
        2 => 'twelve',
        3 => 'thirteen',
        4 => 'fourteen',
        5 => 'fifteen',
        6 => 'sixteen',
        7 => 'seventeen',
        8 => 'eighteen',
        9 => 'nineteen',
    ];

    const TENS = [
        1 => 'ten',
        2 => 'twenty',
        3 => 'thirty',
        4 => 'fourty',
        5 => 'fifty',
        6 => 'sixty',
        7 => 'seventy',
        8 => 'eighty',
        9 => 'ninety',
    ];

    const LEVELS = [
        0 => '',
        1 => 'thousand',
        2 => 'million',
        3 => 'billion',
        4 => 'trillion',
        5 => 'quadrillion',
        6 => 'trylion',
        7 => 'quadrillion',
        8 => 'quintillion',
        9 => 'sextillion',
    ];

    const DECIMAL_LEVELS = [
        1 => 'tenth',
        2 => 'hundrenth',
        3 => 'thousandth',
    ];

    public function wordifyWhole($whole, int $gender)
    {
        if ($whole == 0) {
            return 'zero';
        }

        $out = [];
        $threes = $this->splitNumberInThrees($whole);

        for ($level = count($threes)-1; $level >= 0; $level--) {
            $part = $threes[$level];
            if ($part == 0) {
                continue;
            }
            if ($part > 1 || $level == 0) {
                $out[] = $this->numberPartInWords($part);
            }
            if ($level > 0) {
                $out[] = self::LEVELS[$level];
            }
        }

        return join(' ', $out);
    }

    private function splitNumberInThrees($whole)
    {
        $whole = number_format($whole, 0, '', '');
        $result = [];

        do {
            $end = substr($whole, -3);
            $whole = substr($whole, 0, -3);
            $result[] = $end;
        } while (strlen($whole) > 0);

        return $result;
    }

    private function numberPartInWords($part)
    {
        $out = [];
        $ones = $part % 10;
        $tens = floor($part / 10) % 10;
        $hundreds = floor($part / 100) % 10;

        if ($hundreds) {
            $out[] = self::ONES[$hundreds] . ' hundred';
        }

        if ($tens == 1 && $ones > 0) {
            $out[] = self::TEENS[$ones];
        } elseif ($tens) {
            $out[] = $ones ? (self::TENS[$tens] . '-' . self::ONES[$ones]) : self::TENS[$tens];
        } else {
            $out[] = $ones > 0 ? self::ONES[$ones] : '';
        }

        return trim(join(' ', $out));
    }

    public function wordifyDecimal($decimal, int $gender)
    {
        $decimal = strpos($decimal, 'E') !== false || strpos($decimal, 'e') !== false ? 0 : $decimal;

        if ($decimal == 0) {
            return '';
        }

        return join(' ', strlen($decimal) <= 3
            ? $this->shortDecimalInWords($decimal)
            : $this->longDecimalInWords($decimal));
    }

    private function shortDecimalInWords($decimal)
    {
        return [
            $this->numberPartInWords($decimal),
            self::DECIMAL_LEVELS[strlen($decimal)],
        ];
    }

    private function longDecimalInWords($decimal)
    {
        $out = [];

        for ($i = 0; $i < strlen($decimal); $i++) {
            $out[] = self::ONES[substr($decimal, $i, 1)];
        }

        return $out;
    }
}
