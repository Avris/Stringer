<?php
namespace Avris\Stringer\Service;

final class UrlCleaner
{
    public function clearUrl(string $url): string
    {
        $url = trim($url);
        $url = str_replace(['http://', 'https://', 'http://www.', 'https://www.'], '', $url);
        if (strpos($url, '?')) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        return trim($url, '/');
    }
}
