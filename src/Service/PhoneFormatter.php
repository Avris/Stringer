<?php

namespace Avris\Stringer\Service;

use Avris\Stringer\LocaleFormatter\PhoneLocaleFormatter;

final class PhoneFormatter
{
    const INT_CODE_LENGTH_1 = [1, 7];
    const INT_CODE_LENGTH_3 = [
        21, 22, 23, 24, 25, 26, 29, 35, 37, 38, 42,
        50, 59, 67, 68, 69, 80, 85, 87, 88, 96, 97, 99,
    ];

    /** @var PhoneLocaleFormatter[] */
    private $localeFormatters = [];

    /**
     * @codeCoverageIgnore
     */
    public function __construct(array $phoneLocaleFormatters = [])
    {
        $this->localeFormatters = $phoneLocaleFormatters;
    }

    public function format($number, $defaultLocale = null): string
    {
        list($intCode, $number) = $this->splitPhoneNumber(str_replace(' ', '', $number));

        if (!$intCode && $defaultLocale) {
            foreach ($this->localeFormatters as $formatter) {
                foreach ($formatter->getLocales() as $locale) {
                    if ((string) $locale === (string) $defaultLocale) {
                        $intCode = $formatter->getCode();
                        break;
                    }
                }
            }
        }

        foreach ($this->localeFormatters as $formatter) {
            if ($formatter->getCode() === $intCode) {
                $number = $formatter->format($number);
                break;
            }
        }

        return trim($intCode . ' ' . $number);
    }

    private function splitPhoneNumber($number)
    {
        list($intCode, $number) = $this->findPhoneNumberPlus($number);

        if ($intCode === '+') {
            $len = $this->phoneNumberIntCodeLength($number);
            $intCode .= substr($number, 0, $len);
            $number = substr($number, $len);
        }

        return [$intCode, $number];
    }

    private function findPhoneNumberPlus($number)
    {
        if (substr($number, 0, 2) == '00') {
            return ['+', substr($number, 2)];
        } elseif (substr($number, 0, 1) == '+') {
            return ['+', substr($number, 1)];
        }
        return ['', $number];
    }

    private function phoneNumberIntCodeLength($number)
    {
        if (in_array(substr($number, 0, 1), self::INT_CODE_LENGTH_1)) {
            return 1;
        } elseif (in_array(substr($number, 0, 2), self::INT_CODE_LENGTH_3)) {
            return 3;
        }
        return 2;
    }
}
