<?php
namespace Avris\Stringer\Service;

use Avris\Localisator\LocalisatorInterface;

final class Listify
{
    /** @var LocalisatorInterface */
    private $localisator;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(LocalisatorInterface $localisator)
    {
        $this->localisator = $localisator;
    }
    
    public function listify(array $array, int $maxShown = 0, $base = 'stringer:listify.'): string
    {
        $array = array_values($array);
        $count = count($array);

        if ($maxShown === 0) {
            $maxShown = $count;
        }

        if ($maxShown === 1) {
            return $count === 1
                ? $array[0] . $this->prependSpace($this->localisator->get($base . 'simple', ['count' => 1, 'person' => $array[0]])) // A skomentował
                : $this->localisator->get($base . 'addition', ['count' => $count, 'person' => $array]) ?: $count; // 5 osób skomentowało
        }

        list($arrayShown, $arrayHidden) = $this->splitArray($array, min($count, $maxShown));

        return sprintf(
            '%s %s %s',
            implode($arrayShown, ', '),
            $this->localisator->get('stringer:and'),
            count($arrayHidden) > 1
                ? $this->localisator->get($base . 'others', ['count' => count($arrayHidden), 'person' => $arrayHidden]) ?: count($arrayHidden)  // A, B i 3 inne osoby skomentowały
                : $arrayHidden[0] . $this->prependSpace($this->localisator->get($base . 'simple', ['count' => $count, 'person' => $array])) // A, B i C skomentowali
        );
    }

    /**
     * @return array [array shown, array hidden]
     */
    private function splitArray(array $array, int $limit): array
    {
        return [
            array_slice($array, 0, $limit - 1),
            array_slice($array, $limit - 1)
        ];
    }

    private function prependSpace(?string $string): string
    {
        return trim($string) === '' ? '' : ' ' . $string;
    }
}
