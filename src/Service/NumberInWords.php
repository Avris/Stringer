<?php
namespace Avris\Stringer\Service;

use Avris\Localisator\LocalisatorInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Stringer\LocaleFormatter\MatchLocaleFormatter;
use Avris\Stringer\LocaleFormatter\NumberInWordsLocaleFormatter;
use Avris\Stringer\Stringer;

final class NumberInWords
{
    use MatchLocaleFormatter;

    /** @var LocalisatorInterface */
    private $localisator;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    /** @var NumberInWordsLocaleFormatter[] */
    private $formatters;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(
        LocalisatorInterface $localisator,
        LocaleOrderProviderInterface $orderProvider,
        array $numberInWordsLocaleFormatters
    ) {
        $this->localisator = $localisator;
        $this->orderProvider = $orderProvider;
        $this->formatters = $numberInWordsLocaleFormatters;
    }

    public function numberInWords($number, int $gender = Stringer::MASCULINE): string
    {
        /** @var NumberInWordsLocaleFormatter $formatter */
        $formatter = $this->matchFormatter();

        list($minus, $number) = $this->handleSign($number);
        list($whole, $decimal) = $this->splitWholeDecimal($number);
        $decimal = $this->repairExponential($decimal);
        $decimal = $this->repairRoundedNonMachine($number, $decimal);

        $wholeInWords = $formatter->wordifyWhole($whole, $gender);
        $decimalInWords = $formatter->wordifyDecimal($decimal, $gender);

        return $this->joinWholeDecimal($minus, $whole, $wholeInWords, $decimal, $decimalInWords);
    }

    private function handleSign($number)
    {
        return substr($number, 0, 1) === '-'
            ? [true, substr($number, 1)]
            : [false, $number];
    }

    private function splitWholeDecimal($number)
    {
        $whole = floor($number);
        $decimal = $number - $whole;

        return [$whole, $decimal];
    }

    private function repairExponential($decimal)
    {
        if (strpos($decimal, 'E') !== false || strpos($decimal, 'e') !== false) {
            $minusPos = strrpos($decimal, '-');
            $mantissa = substr($decimal, 0, $minusPos-1);
            $exponent = (int)substr($decimal, $minusPos+1);
            $decimal = number_format($decimal, strlen($mantissa)-2 + $exponent);
        }

        return $decimal;
    }

    private function repairRoundedNonMachine($number, $decimal)
    {
        if ($decimal > 0 && preg_match('/(.+)E-(.*)/', $number, $matches)) {
            $len = strlen($matches[1]) + $matches[2] - 2;
            return substr(number_format($number, $len), 2);
        }

        return $decimal > 0
            ? $decimal = rtrim(substr($number, strpos($number, '.') + 1), '0')
            : $decimal;
    }

    private function joinWholeDecimal($minus, $whole, $wholeInWords, $decimal, $decimalInWords)
    {
        $out = $decimal
            ? (strlen($decimal) <= 3
                ? ($whole
                    ? $wholeInWords . ' ' . $this->localisator->get('stringer:and') . ' ' . $decimalInWords
                    : $decimalInWords)
                : $wholeInWords . ' ' . $this->localisator->get('stringer:number.comma') . ' ' . $decimalInWords)
            : $wholeInWords;

        return $minus ? $this->localisator->get('stringer:number.minus') . ' ' . $out : $out;
    }
}
