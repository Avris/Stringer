<?php
namespace Avris\Stringer\Service;

final class CaseConverter
{
    const CASE_TITLE = 'title';
    const CASE_CAMEL = 'camel';
    const CASE_PASCAL = 'pascal';
    const CASE_UNDERSCORE = 'underscore';

    public function convertCase(string $string, string $format): string
    {
        $method = 'convertTo' . ucfirst(strtolower($format));

        return method_exists($this, $method)
            ? call_user_func([$this, $method], $string)
            : $string;
    }

    /**
     * Convert To Title Case
     */
    private function convertToTitle(string $string): string
    {
        $string = ucfirst(trim($string));
        $string = preg_replace_callback(
            ['/([A-Z])/', '/ ([a-z])/', '/_([a-z])/'],
            function ($char) {
                return ' ' . strtoupper($char[1]);
            },
            $string
        );

        return trim(preg_replace('/\s+/', ' ', $string));
    }

    /**
     * convertToCamelCase
     */
    private function convertToCamel(string $string): string
    {
        $string = lcfirst(trim($string));
        $string = preg_replace_callback(
            ['/_([a-z])/', '/ ([a-z])/'],
            function ($char) {
                return strtoupper($char[1]);
            },
            $string
        );

        return str_replace(' ', '', $string);
    }

    /**
     * ConvertToPascalCase
     */
    private function convertToPascal(string $string): string
    {
        $string = ucfirst(trim($string));
        $string = preg_replace_callback(
            ['/_([a-z])/','/ ([a-z])/'],
            function ($char) {
                return strtoupper($char[1]);
            },
            $string
        );

        return str_replace(' ', '', $string);
    }

    /**
     * convert_to_underscore
     */
    private function convertToUnderscore(string $string): string
    {
        $string = lcfirst(trim($string));
        $string = preg_replace_callback(
            ['/([A-Z])/','/ ([a-z])/'],
            function ($char) {
                return '_' . strtolower($char[1]);
            },
            $string
        );

        return str_replace(' ', '', $string);
    }

    public function guessCase(string $string): string
    {
        if (strpos($string, ' ') !== false) {
            return self::CASE_TITLE;
        }

        if (strpos($string, '_') !== false) {
            return self::CASE_UNDERSCORE;
        }

        return substr($string, 0, 1) === strtoupper(substr($string, 0, 1))
            ? self::CASE_PASCAL
            : self::CASE_CAMEL;
    }
}
