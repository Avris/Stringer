<?php
namespace Avris\Stringer\Service;

use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Stringer\Entity\Time;
use Avris\Stringer\LocaleFormatter\MatchLocaleFormatter;
use Avris\Stringer\LocaleFormatter\TimeInWordsLocaleFormatter;

final class TimeInWords
{
    use MatchLocaleFormatter;

    const MODE_NICE = 0;
    const MODE_SHORT = 1;
    const MODE_LONG = 2;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    /** @var TimeInWordsLocaleFormatter[] */
    private $formatters;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(
        LocaleOrderProviderInterface $orderProvider,
        array $timeInWordsLocaleFormatters
    ) {
        $this->orderProvider = $orderProvider;
        $this->formatters = $timeInWordsLocaleFormatters;
    }

    public function timeInWords($time, $mode = self::MODE_NICE): string
    {
        /** @var TimeInWordsLocaleFormatter $formatter */
        $formatter = $this->matchFormatter();

        $time = new Time($time);

        if ($mode === self::MODE_SHORT) {
            return $formatter->getShort($time);
        } elseif ($mode === self::MODE_LONG) {
            return $formatter->getLong($time);
        }

        return $formatter->getNice($time);
    }
}
