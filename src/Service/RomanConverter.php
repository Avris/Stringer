<?php
namespace Avris\Stringer\Service;

final class RomanConverter
{
    const MIN = 1;
    const MAX = 3999;

    public function arabicToRoman(int $arabic): string
    {
        if ($arabic < self::MIN) {
            throw new \InvalidArgumentException(sprintf('Input value must be %d or greater', self::MIN));
        }

        if ($arabic > self::MAX) {
            throw new \InvalidArgumentException(sprintf('Input value must be %d or less', self::MAX));
        }

        list($thousands, $hundreds, $tens, $ones) = $this->arabicToRomanGetPositions($arabic);

        return
            $this->romanConvertDigit((int) $thousands, 'M', '', '')  .
            $this->romanConvertDigit((int) $hundreds, 'C', 'D', 'M') .
            $this->romanConvertDigit((int) $tens, 'X', 'L', 'C') .
            $this->romanConvertDigit((int) $ones, 'I', 'V', 'X');
    }

    private function arabicToRomanGetPositions($number)
    {
        return str_split(str_pad($number, 4, '0', STR_PAD_LEFT));
    }

    private function romanConvertDigit(int $digit, string $ones, string $fives, string $tens): string
    {
        if ($digit == 0) {
            return '';
        } elseif ($digit <= 3) {
            return $this->mergeRoman('', $ones, $digit);
        } elseif ($digit >= 4 && $digit <= 8) {
            return $this->mergeRoman($fives, $ones, $digit - 5);
        }

        return $this->mergeRoman($tens, $ones, -1);
    }

    private function mergeRoman(string $main, string $aside, int $times): string
    {
        if ($times < 0) {
            return str_repeat($aside, -$times) . $main;
        } elseif ($times == 0) {
            return $main;
        }

        return $main . str_repeat($aside, $times);
    }
}
