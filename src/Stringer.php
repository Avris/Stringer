<?php
namespace Avris\Stringer;

use Avris\Stringer\Service\Listify;
use Avris\Stringer\Service\CaseConverter;
use Avris\Stringer\Service\NumberInWords;
use Avris\Stringer\Service\PhoneFormatter;
use Avris\Stringer\Service\RomanConverter;
use Avris\Stringer\Service\TimeDiff;
use Avris\Stringer\Service\TimeInWords;
use Avris\Stringer\Service\UrlCleaner;

final class Stringer
{
    const MASCULINE = 0;
    const FEMININE = 1;

    /** @var Listify */
    private $listify;

    /** @var CaseConverter */
    private $caseConverter;

    /** @var RomanConverter */
    private $romanConverter;

    /** @var UrlCleaner */
    private $urlCleaner;

    /** @var PhoneFormatter */
    private $phoneFormatter;

    /** @var NumberInWords */
    private $numberInWords;

    /** @var TimeInWords */
    private $timeInWords;

    /** @var TimeDiff */
    private $timeDiff;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(
        Listify $listify,
        CaseConverter $caseConverter,
        RomanConverter $romanConverter,
        UrlCleaner $urlCleaner,
        PhoneFormatter $phoneFormatter,
        NumberInWords $numberInWords,
        TimeInWords $timeInWords,
        TimeDiff $timeDiff
    ) {
        mb_internal_encoding('UTF-8');

        $this->listify = $listify;
        $this->caseConverter = $caseConverter;
        $this->romanConverter = $romanConverter;
        $this->urlCleaner = $urlCleaner;
        $this->phoneFormatter = $phoneFormatter;
        $this->numberInWords = $numberInWords;
        $this->timeInWords = $timeInWords;
        $this->timeDiff = $timeDiff;
    }

    public function listify(array $array, int $maxShown = 0, $base = 'stringer:listify.'): string
    {
        return $this->listify->listify($array, $maxShown, $base);
    }

    public function convertCase(string $string, string $format): string
    {
        return $this->caseConverter->convertCase($string, $format);
    }

    public function guessCase(string $string): string
    {
        return $this->caseConverter->guessCase($string);
    }

    public function arabicToRoman(int $arabic): string
    {
        return $this->romanConverter->arabicToRoman($arabic);
    }

    public function clearUrl(string $url): string
    {
        return $this->urlCleaner->clearUrl($url);
    }

    public function phone($number, $defaultLocale = null): string
    {
        return $this->phoneFormatter->format($number, $defaultLocale);
    }

    public function numberInWords($number, int $gender = self::MASCULINE): string
    {
        return $this->numberInWords->numberInWords($number, $gender);
    }

    public function timeInWords($time, $mode = TimeInWords::MODE_NICE): string
    {
        return $this->timeInWords->timeInWords($time, $mode);
    }

    public function timeDiff($datetime, $now = null): string
    {
        return $this->timeDiff->diff($datetime, $now);
    }
}
