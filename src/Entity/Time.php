<?php
namespace Avris\Stringer\Entity;

final class Time
{
    /** @var int */
    private $hours;

    /** @var int */
    private $minutes;

    /** @var int */
    private $seconds;

    /** @var int */
    private $hoursNext;

    public function __construct($time)
    {
        $time = $time instanceof \DateTime ? $time : new \DateTime($time);

        $this->hours = (int) $time->format('H');
        $this->minutes = (int) $time->format('i');
        $this->seconds = (int) $time->format('s');
        $this->hoursNext = ($this->hours + 1) % 24;
    }

    public function getHours(): int
    {
        return $this->hours;
    }

    public function getMinutes(): int
    {
        return $this->minutes;
    }

    public function getSeconds(): int
    {
        return $this->seconds;
    }

    public function getHoursNext(): int
    {
        return $this->hoursNext;
    }
}
